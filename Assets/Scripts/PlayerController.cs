﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    private float moveVelocity;
    public float jumpHeight;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private bool grounded;

    private bool doubleJumped;

    private Animator anim;

    public Transform firePoint;
    public GameObject laserBeam;

    public float shotDelay;
    private float shotDelayCounter;

    public float knockBack;
    public float knockJump;
    public float knockBackLength;
    public float knockBackCount;
    public bool knockFromRight;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
	}
	
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

	// Update is called once per frame
	void Update () {

        if (grounded)
            doubleJumped = false;

        anim.SetBool("Grounded", grounded);
            
            if (Input.GetKeyDown (KeyCode.UpArrow) && grounded)
        {
            Jump();
           // GetComponent<Rigidbody2D>().velocity = new Vector2(0, jumpHeight);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && !doubleJumped && !grounded)
        {
            Jump();
           // GetComponent<Rigidbody2D>().velocity = new Vector2(0, jumpHeight);
            doubleJumped = true;
        }

        moveVelocity = 0f;

        if (Input.GetKey(KeyCode.RightArrow))//right
        {
            // GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            moveVelocity = moveSpeed;
        }

        if (Input.GetKey(KeyCode.LeftArrow))//left
        {
            //GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            moveVelocity = -moveSpeed;
        }

        if (knockBackCount <= 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            if (knockFromRight)
                GetComponent<Rigidbody2D>().velocity = new Vector2 (-knockBack, knockJump);
            
            if (!knockFromRight)
                GetComponent<Rigidbody2D>().velocity = new Vector2(knockBack, knockJump);
            knockBackCount -= Time.deltaTime;
            
        }

        anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));

        if (GetComponent<Rigidbody2D>().velocity.x > 0)
            transform.localScale = new Vector3(1f, 1f, 1f);

        else if(GetComponent<Rigidbody2D>().velocity.x < 0)
            transform.localScale = new Vector3(-1f, 1f, 1f);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(laserBeam, firePoint.position, firePoint.rotation);
            shotDelayCounter = shotDelay;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            shotDelayCounter -= Time.deltaTime;

            if(shotDelayCounter <= 0)
            {
                shotDelayCounter = shotDelay;
                Instantiate(laserBeam, firePoint.position, firePoint.rotation);
            }
        }

    }

    public void Jump()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, jumpHeight);
    }
}
