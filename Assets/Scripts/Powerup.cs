﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

    public int pointsToAdd;

    public AudioSource soundEffect;

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.GetComponent<PlayerController>() == null)
            return;

        ScoreManager.AddPoints(pointsToAdd);

        soundEffect.Play();

        Destroy(gameObject);
    }
}
