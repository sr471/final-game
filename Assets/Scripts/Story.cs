﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Story : MonoBehaviour
{


    public GameObject slide1;

    public GameObject slide2;

    public GameObject slide3;

    public GameObject slide4;

    // public PlayerController player;

    public string scence;

    public float waitTime1;
    public float waitTime2;
    public float waitTime3;
    public float waitTime4;

    // Use this for initialization
    void Start()
    {

     //   player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

        slide1.SetActive(true);
        slide2.SetActive(false);
        slide3.SetActive(false);
        slide4.SetActive(false);

        //  player.gameObject.SetActive(false);

        if (slide1.activeSelf)
        {
            waitTime1 -= Time.deltaTime;
        }

        if (waitTime1 < 0)
        {
            slide1.SetActive(false);
            slide2.SetActive(true);
        }

        if (slide2.activeSelf)
        {
            waitTime2 -= Time.deltaTime;
        }

        if (waitTime2 < 0)
        {
            slide2.SetActive(false);
            slide3.SetActive(true);
        }

        if (slide3.activeSelf)
        {
            waitTime3 -= Time.deltaTime;
        }

        if (waitTime3 < 0)
        {
            slide3.SetActive(false);
            slide4.SetActive(true);
        }

        if (slide4.activeSelf)
        {
            waitTime4 -= Time.deltaTime;
        }

        if (waitTime4 < 0)
        {
            slide4.SetActive(false);
          //  player.gameObject.SetActive(true);
            Application.LoadLevel(scence);
        }
    }

}